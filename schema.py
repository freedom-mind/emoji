from dataclasses import dataclass

@dataclass
class MatchElement:
    """Индекс и значения найденого элемента в тексте."""
    start: int
    end: int
    value: str
