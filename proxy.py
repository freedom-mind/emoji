import aiohttp
import logging
import sys
from urllib.parse import urljoin

from aiohttp import web
from helpers import get_indexes, add_emoji, BASE_URL, LETTERS_COUNT

logger = logging.getLogger("proxy")


async def proxy(request):
    url = urljoin(BASE_URL, request.match_info['path'])
    async with aiohttp.ClientSession() as session:
        async with session.request(request.method, url) as res:
            html = await res.text()
    matches = get_indexes(html, LETTERS_COUNT)
    html = add_emoji(matches, html)
    return web.Response(text=html, status=res.status, content_type='text/html')


if __name__ == "__main__":
    logging.root.setLevel(logging.INFO)
    logging.root.addHandler(logging.StreamHandler(sys.stdout))

    app = web.Application()
    app.router.add_route('*', '/{path:.*?}', proxy)
    web.run_app(app, host='127.0.0.1', port=9000)
