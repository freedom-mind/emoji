import re
from typing import List
from schema import MatchElement

EMOJI = ['&#128514;', '&#129300;', '&#128126;', '&#129302;', '&#128570;']
BASE_URL = 'https://habr.com/ru/'
LETTERS_COUNT = 6


def get_indexes(text: str, n: int) -> List[MatchElement]:
    """Найти слова с длинной равной n, а так же индексы этих слов.

    :param text: исходный текст
    :param n: длина слова
    :return: структура MatchElement с информацией о найденых словах
    """
    letters_count = '{%s}' % n
    regexp = fr'\b[а-яА-Я]{letters_count}\b'
    items = []
    for match in re.finditer(regexp, text):
        items.append(MatchElement(
            start=match.start(),
            end=match.end(),
            value=match.group(),
        ))

    return items


def get_replaced_text(text, item: MatchElement, emoji_idx, idx_shift):
    """Замена слова в тексте на такое же слово, но с emoji.

    :param text:  исходный текст
    :param item: заменяемый элемент
    :param emoji_idx: индекс необходимого эмодзи в константе EMOJI
    :param idx_shift: сдвиг индекса в тексте
    :return:
    """
    return text[:item.start + idx_shift] + item.value + EMOJI[emoji_idx] + text[item.end + idx_shift:]


def add_emoji(indexes: List[MatchElement], text: str) -> str:
    """Добавить эмодзи в текст.

    :param indexes: найденые слова и их индексы в тексте
    :param text: исходный текст
    :return: текст с эмодзи
    """

    idx_shift = 0
    emoji_idx = 0
    emoji_length = [len(em) for em in EMOJI]
    max_emoji_idx = len(EMOJI) - 1

    for item in indexes:
        text = get_replaced_text(text, item, emoji_idx, idx_shift)
        idx_shift += emoji_length[emoji_idx]
        if emoji_idx < max_emoji_idx:
            emoji_idx += 1
        else:
            emoji_idx = 0
    return text
